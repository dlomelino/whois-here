import os
import json
import requests
import time
from requests_oauthlib import OAuth2Session
from flask import Flask, request, redirect, session, url_for, render_template
from flask.json import jsonify
from config import CLIENT_ID, CLIENT_SECRET, REDIRECT_URI

API_AUTH_BASE = 'https://secure.meetup.com/oauth2'
AUTHORIZATION_URI = API_AUTH_BASE + '/authorize'
TOKEN_URI = API_AUTH_BASE + '/access'
API_BASE = 'https://api.meetup.com'
SELF_URI = API_BASE + '/2/member/self'
SELF_EVENTS_URI = API_BASE + '/self/events'
RSVPS_URI = API_BASE + '/2/rsvps'

app = Flask(__name__)


@app.route('/')
def demo():
    """
    Redirect the user to the OAuth provider in order to get an access code for
    token retrieval.
    """
    meetup = OAuth2Session(CLIENT_ID, redirect_uri=REDIRECT_URI)
    authorization_uri, state = meetup.authorization_url(AUTHORIZATION_URI)

    session['oauth_state'] = state

    return redirect(authorization_uri)


def refresh_token():
    resp = requests.post(TOKEN_URI, data = { 'client_id': CLIENT_ID,
                                            'client_secret': CLIENT_SECRET,
                                            'grant_type': 'refresh_token',
                                            'refresh_token': session['oauth_refresh_token'] })

    return resp.text

@app.route('/callback', methods=['GET'])
def callback():
    """
    Callback handler for when the OAuth provider redirects back to our
    redirect_uri with a code for fetching a token.

    Get a token with the code returned from the OAuth provider and store it in
    the session.
    """
    # grab the authorization code from the OAuth provider
    code = request.args.get('code')
    print('CODE', code)
    # get a token from the OAuth provider; the usual
    # OAuth2Session.fetch_token() method doesn't seem to work with Meetup's API
    token_resp = requests.post(TOKEN_URI,
                               data = { 'client_id': CLIENT_ID,
                                       'client_secret': CLIENT_SECRET,
                                       'grant_type': 'authorization_code',
                                       'code': code,
                                       'redirect_uri': REDIRECT_URI })
    # get response in JSON format
    resp_json = token_resp.json()
    # store tokens in session
    session['oauth_access_token'] = resp_json['access_token']
    session['oauth_refresh_token'] = resp_json['refresh_token']

    return redirect(url_for('.profile'))


def api_call(uri, method='GET', args_data={}):
    access_token = session['oauth_access_token']

    # the usual OAuth2Session.get() doesn't seem to work with Meetup's API
    if method.upper() == 'GET':
        # add the access token to the GET query string
        uri += '?access_token={}'.format(access_token)
        # add the passed-in request parameters as a query string for GET
        # requests
        for k, v in args_data.items():
            uri += '&{}={}'.format(k, v)
        resp = requests.get(uri, data = args_data)
    elif method.upper() == 'POST':
        # add the access token to the payload for POST requests
        data = { 'access_token': access_token }
        # add the passed-in request parameters to the payload for POST requests
        data.update(args_data)
        resp = requests.post(uri, data = data)
    else:
        resp = None

    return json.loads(resp.text)

@app.route('/profile')
def profile():
    """
    Entry point for the UI. Main page for issuing API calls.
    """

    data = {
        # 'status': 'upcoming',
        'rsvp': 'yes',
    }
    # API call wrapper to allow for refreshing access tokens
    events = api_call(SELF_EVENTS_URI, args_data=data)
    nowtime = time.time()

    # for each event
    for d in events:
        event_start = d['time']
        if 'duration' in d.keys():
            event_end = d['time'] + d['duration']
        else:
            event_end = d['time'] + (((3 * 60) * 60) * 1000) # milliseconds

        # use the following conditional to only show events that are currently happening
        # if event_start <= nowtime <= event_end:

    return render_template('events.html', events=events)


@app.route('/event/<id>')
def event(id):
    urlname = request.args.get('urlname')
    event = api_call('{0}/{1}/events/{2}'.format(API_BASE, urlname, id))
    event_id = event['id']
    data = {
        'event_id': event_id,
        'fields': 'host,member_bio',
    }
    rsvps = api_call(RSVPS_URI, args_data=data)['results']
    for rsvp in rsvps:
        print(rsvp, end='\n\n')
    context = {
        'event': event,
        'rsvps': rsvps,
    }

    return render_template('event.html', context=context)


if __name__ == '__main__':
    # allow local testing of OAuth2 without https
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
    os.environ['DEBUG'] = '1'

    app.secret_key = os.urandom(24)

    app.run(debug=True, port=8000)

